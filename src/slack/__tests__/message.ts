jest.mock("../../handlers/feed");
jest.mock("../../handlers/package");

import mongodb = require("mongo-mock");
import {addFeed, listFeeds, removeFeed, updateFeeds} from "../../handlers/feed";
import {addPackage, listPackages, removePackage, updatePackages} from "../../handlers/package";
import {handleMessage} from "../message";

const self = {
    id: "bot1",
    name: "bot",
};

const cfg = {
    mongoName: "test",
    mongoUrl: "test",
    slackApiToken: "1234",
};

const msg = {
    channel: "test-channel",
    type: "message",
};

test("handle message", async () => {
    const db = await mongodb.MongoClient.connect("fakeurl_scanner_1", {});

    handleMessage(db, self, {...msg, text: "<@bot1> list packages"}, cfg);
    expect((listPackages as any).mock.calls).toEqual([[db.collection("packages"), "test-channel"]]);
});

test("unknown message", async () => {
    const db = await mongodb.MongoClient.connect("fakeurl_scanner_1", {});

    expect.assertions(1);
    return handleMessage(db, self, {...msg, text: "some unhandled message"}, cfg).
        catch((error) => {
            expect(error).toBe("No response");
        });
});
