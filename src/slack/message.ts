import {Db} from "mongodb";
import {handlers} from "../handlers";
import {IConfig} from "../utils/config";
import {IMessage, ISelf} from "./types";

export function handleMessage(db: Db, self: ISelf, msg: IMessage, config: IConfig): Promise<string> {
    /** Loop over the handlers and call it if pattern matches. Returns Promise with return message */
    if (msg.text && !msg.bot_id && msg.subtype !== "bot_message" && msg.user !== self.id &&
        (msg.channel.startsWith("D") || msg.text.includes(`<@${self.id}>`))) {

        for (const [pattern, handler] of handlers) {
            const matches = new RegExp(pattern).exec(msg.text);
            if (matches) {
                return handler(db, msg, config, matches);
            }
        }
    }
    return Promise.reject("No response");
}
