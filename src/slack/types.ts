export interface IChannel {
    id: string;
    name: string;
}

export interface IMessage {
    type: string;
    subtype?: string;
    channel: string;
    text?: string;
    user: string;
    message?: {
        text: string;
    };
    bot_id?: string;
}

export interface ISelf {
    id: string;
    name: string;
}
