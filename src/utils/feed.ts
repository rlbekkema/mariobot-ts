import * as cheerio from "cheerio";

export function cleanHtml(rawHtml: string): string {
    if (!rawHtml) {
        return rawHtml;
    }

    const $ = cheerio.load(rawHtml);

    // Replace all links with href
    $("a").each((i, elem) => {
        $(elem).replaceWith(elem.attribs.href);
    });

    // Remove images
    $("img").replaceWith("");

    return $("body").html() || "";
}
