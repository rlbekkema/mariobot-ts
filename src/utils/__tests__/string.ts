import { deSlackify, ucFirst } from "../string";

test("capitalizes first letter of string", () => {
    expect(ucFirst("test")).toBe("Test");
    expect(ucFirst("Test")).toBe("Test");
    expect(ucFirst("test test")).toBe("Test test");
});

test("removes slack junk from link in text", () => {
    expect(deSlackify("<http://test.test>")).toBe("http://test.test");
    expect(deSlackify("test")).toBe("test");
});
