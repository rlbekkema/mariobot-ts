import {format} from "date-fns";

jest.mock("../../packages/finders", () => ({
    findVersion: jest.fn().mockImplementation((language, name) => {
        return new Promise((resolve) => {
            resolve({ name, title: name, version: "2", releaseUrl: "http://test" });
        });
    }),
}));

const mockDate = new Date(2018, 0, 1);
jest.mock("../../feeds/feed", () => ({
    updateFeed: jest.fn().mockImplementation(() => {
        return Promise.resolve({
            items: [{title: "Test Item", published: mockDate, link: "http://test", content: "Test Content"}],
            title: "Test Title",
        });
    }),
}));

const fakeConfig: IConfig = {
    mongoName: "",
    mongoUrl: "",
    slackApiToken: "1234",
};

// noinspection TsLint
const mongodb = require("mongo-mock");
import { Db } from "mongodb";
import * as nock from "nock";
import { IConfig } from "../config";
import { scan, scanFeeds, scanPackages } from "../scanner";

test("scans pacakges", async () => {
    const db: Db = await mongodb.MongoClient.connect("fakeurl_scanner_1", {});
    const packages = db.collection("packages");

    const expectedBody = "as_user=true&channel=test-channel&text=_Test_%20%2F%20*package1*%20update%3A%20%3C" +
        "http%3A%2F%2Ftest%7C_2_%3E&token=1234";
    nock("https://slack.com")
        .post("/api/chat.postMessage", expectedBody)
        .reply(200, "ok");

    const packge = {language: "test", name: "package1", channel: "test-channel"};
    await packages.insertOne({...packge, version: "1"});
    await scanPackages(db, fakeConfig);

    const row = await packages.findOne(packge);

    expect(row.version).toBe("2");
    expect(nock.isDone()).toBe(true); // Slack message was posted
});

test("scans feeds", async () => {
    const db: Db = await mongodb.MongoClient.connect("fakeurl_scanner_2", {});
    const feeds = db.collection("feeds");
    const fdate = format(mockDate, "ddd D MMM HH:mm");
    const expectedBody = `as_user=true&channel=test_channel&text=*Test%20Item*%20(Update%3A%20_` +
        `${encodeURIComponent(fdate)}_)%0Ahttp%3A%2F%2Ftest%0A&token=1234`;
    nock("https://slack.com")
        .post("/api/chat.postMessage", expectedBody)
        .reply(200, "ok");

    const packge = {url: "http://test", channel: "test_channel"};
    await feeds.insertOne({...packge, lastUpdate: new Date(2017, 0, 1)});
    await scanFeeds(db, fakeConfig);

    const row = await feeds.findOne(packge);

    expect(row.lastUpdate).toEqual(mockDate);
    expect(nock.isDone()).toBe(true); // Slack message was posted
});

test("scan both", async () => {
    const db: Db = await mongodb.MongoClient.connect("fakeurl_scanner_3", {});
    const voids = await scan(db, fakeConfig);

    expect(voids).toBe(undefined);
});
