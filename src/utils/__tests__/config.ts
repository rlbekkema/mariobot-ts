import * as dotenv from "dotenv";
import {getConfig} from "../config";
jest.mock("dotenv");

beforeEach(() => {
    jest.resetModules();
});

test("config works", () => {
    process.env.MARIOBOT_MONGO_NAME = "test";
    process.env.MARIOBOT_MONGO_URL = "mongodb://test";
    process.env.MARIOBOT_SLACK_API_TOKEN = "1234";

    const config = getConfig();

    expect(config).toEqual({
        mongoName: "test", mongoUrl: "mongodb://test", slackApiToken: "1234",
    });
});

test("missing value", () => {
    process.env.MARIOBOT_MONGO_NAME = "test";
    process.env.MARIOBOT_MONGO_URL = "mongodb://test";
    delete process.env.MARIOBOT_SLACK_API_TOKEN;

    expect(() => {
        getConfig();
      }).toThrow("Set MARIOBOT_SLACK_API_TOKEN please");
});
