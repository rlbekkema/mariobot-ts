import * as dotenv from "dotenv";
import _ = require("lodash");

dotenv.config();

export interface IConfig {
    mongoName: string;
    mongoUrl: string;
    slackApiToken: string;
}

export function getConfig(): IConfig {
    const config: IConfig = {
        mongoName: "",
        mongoUrl: "",
        slackApiToken: "",
    };

    for (const name in config) {
        if (config.hasOwnProperty(name)) {
            const envName = "MARIOBOT_" + _.snakeCase(name).toUpperCase();
            const value = process.env[envName];
            if (!value) {
                throw new Error(`Set ${envName} please`);
            }
            (config as any)[name] = value;
        }
    }

    return config;
}
