export function ucFirst(text: string) {
    return text.charAt(0).toUpperCase() + text.slice(1);
}

export function deSlackify(text: string) {
    const match = /^<(.*)>$/.exec(text);
    if (match) {
        return match[1];
    }
    return text;
}
