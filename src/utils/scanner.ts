import {addHours, differenceInMilliseconds, format, parse, startOfHour} from "date-fns";
import {Collection, Db} from "mongodb";
import * as slack from "slack";
import {updateFeed} from "../feeds/feed";
import {findVersion} from "../packages/finders";
import {IVersionInfo} from "../packages/types";
import {IConfig} from "./config";
import {ucFirst} from "./string";

enum ErrorReason {
    NotFound,
    NoUpdate,
}

function processVersion(packages: Collection, info: IVersionInfo,
                        packge: { name: string, version: string, language: string }): Promise<string> {
    if (info.version !== packge.version) {
        const filter = {language: packge.language, name: packge.name};
        const update = {$set: info};
        // Update the database
        return packages.updateMany(filter, update)
            .then(() => {
                return `_${ucFirst(packge.language)}_ / *${info.title}* update: <${info.releaseUrl}|_${info.version}_>`;
            })
            .catch(() => {
                throw ErrorReason.NotFound;
            });
    }
    throw ErrorReason.NoUpdate;
}

export function scanPackages(db: Db, config: IConfig): Promise<void> {
    console.log("Scanning for package updates");
    const packages = db.collection("packages");
    return packages.find().toArray()
        .then(async (packageList) => {
            for (const packge of packageList) {
                // Process one by one
                const imsg = await findVersion(packge.language, packge.name)
                    .then((info) => {
                        return processVersion(packages, info, packge)
                            .then((message) => ({message, channel: packge.channel}));
                    })
                    .catch((reason) => {
                        if (reason === ErrorReason.NotFound) {
                            return {
                                channel: packge.channel,
                                message: `${packge.language} ${packge.name} not found`,
                            };
                        } else {
                            return null;
                        }
                    });
                if (imsg !== null) {
                    slack.chat.postMessage({
                        as_user: true,
                        channel: imsg.channel,
                        text: imsg.message,
                        token: config.slackApiToken,
                    })
                        .catch((error) => console.error(error));
                }
            }
            console.log("Done scanning packages");
        });
}

export function scanFeeds(db: Db, config: IConfig, channel?: string): Promise<void> {
    console.log("Scanning feeds");
    const feeds = db.collection("feeds");
    const filter = channel ? {channel} : {};
    return feeds.find(filter).toArray()
        .then(async (feedList) => {
            for (const feed of feedList) {
                await updateFeed(feed.url, feed.lastUpdate)
                    .then((freshFeed) => {
                        let text = "";
                        freshFeed.items.forEach((feedItem) => {
                            const fdate = format(feedItem.published, "ddd D MMM HH:mm");
                            text += `*${feedItem.title}* (Update: _${fdate}_)\n${feedItem.link}\n`;
                        });
                        if (text) {
                            slack.chat.postMessage({
                                as_user: true,
                                channel: feed.channel,
                                text,
                                token: config.slackApiToken,
                            })
                                .catch((error) => console.error(error));
                        }
                        if (freshFeed.items.length > 0) {
                            return feeds.updateOne({_id: feed._id},
                                {$set: {lastUpdate: parse(freshFeed.items[freshFeed.items.length - 1].published)}});
                        } else {
                            return Promise.resolve(null);
                        }
                    });
            }
            console.log("Done scanning feeds");
        });
}

/**
 * Scan packages and feeds and post the results to Slack
 * @param {Db} db
 * @param {string} config App configuration
 */
export async function scan(db: Db, config: IConfig): Promise<void> {
    await scanPackages(db, config);
    await scanFeeds(db, config);
}

export function startScanner(db: Db, config: IConfig) {
    const now = new Date();
    const theHour = addHours(startOfHour(now), 1);
    const diff = differenceInMilliseconds(theHour, now);

    setTimeout(async () => {
        await scan(db, config)
            .catch((error) => console.log(error));
        startScanner(db, config);
    }, diff);
}
