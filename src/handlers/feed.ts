import {format} from "date-fns";
import {Collection} from "mongodb";
import {updateFeed} from "../feeds/feed";

export function addFeed(feeds: Collection, url: string, channel: string): Promise<string> {
    return feeds.findOne({url, channel})
        .then((row) => {
            if (row === null) {
                // Find version in registry
                return updateFeed(url)
                    .then((feed) => {
                        // Insert it into the database
                        const obj = {
                            channel,
                            lastUpdate: feed.items ? feed.items[feed.items.length - 1].published : null,
                            title: feed.title,
                            url,
                        };
                        return feeds.insertOne(obj).then(() => {
                            return feed;
                        });
                    });
            } else {
                // Row already exists
                return Promise.resolve(null);
            }
        })
        .then((feed) => {
            let msg;
            if (feed !== null) {
                msg = "Feed added";
            } else {
                msg = "Already in the list";
            }
            return msg;
        })
        .catch(() => {
            return "Couldn't fetch feed";
        });
}

export function removeFeed(feeds: Collection, url: string, channel: string): Promise<string> {
    return feeds.findOne({url, channel})
        .then((row) => {
            if (row === null) {
                return Promise.resolve(null);
            } else {
                return feeds.deleteMany({url, channel});
            }
        })
        .then((data) => {
            const response = data === null ? `Feed not found` : `I removed it`;
            return Promise.resolve(response);
        });
}

export function listFeeds(feeds: Collection, channel: string): Promise<string> {
    return feeds
        .find({channel}).sort({title: 1, url: 1}).toArray()
        .then((items) => {
            let text = "";
            for (const item of items) {
                const fdate = format(item.lastUpdate, "ddd D MMM HH:mm");
                text += `*${item.title}* (Last update: _${fdate}_)\n${item.url}\n`;
            }
            if (items.length === 0) {
                text += "Feed list empty";
            }
            return Promise.resolve(text);
        });
}
