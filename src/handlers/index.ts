import {Db} from "mongodb";
import {packageFinders} from "../packages/finders";
import {IMessage} from "../slack/types";
import {IConfig} from "../utils/config";
import {scan} from "../utils/scanner";
import {deSlackify} from "../utils/string";
import {addFeed, listFeeds, removeFeed} from "./feed";
import {addPackage, listPackages, removePackage} from "./package";

export type Handler = [string, (db: Db, msg: IMessage, config: IConfig, matches: string[]) => Promise<string>];
export type Handlers = Handler[];

const langs = Object.keys(packageFinders);

function help(): Promise<string> {
    let msg = "";
    for (const [pattern] of handlers) {
        msg += `@mariobot ${pattern}\n`;
    }
    return Promise.resolve(msg);
}

export const handlers: Handlers = [
    [`add (${langs.join("|")}) package ([\\w-@/]+)`,
        (db, msg, cfg, matches) => addPackage(db.collection("packages"), matches[1], matches[2], msg.channel)],
    [`remove (${langs.join("|")}) package ([\\w-@/]+)`,
        (db, msg, cfg, matches) => removePackage(db.collection("packages"), matches[1], matches[2], msg.channel)],
    ["list packages", (db, msg) => listPackages(db.collection("packages"), msg.channel)],
    ["add feed (.+)",
        (db, msg, cfg, matches) => addFeed(db.collection("feeds"), deSlackify(matches[1]), msg.channel)],
    ["remove feed (.+)",
        (db, msg, cfg, matches) => removeFeed(db.collection("feeds"), deSlackify(matches[1]), msg.channel)],
    ["list feeds", (db, msg) => listFeeds(db.collection("feeds"), msg.channel)],
    ["scan", (db, msg, cfg) => scan(db, cfg).then(() => "Done")],
    [".*", () => help()],
];
