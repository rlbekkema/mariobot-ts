import {Collection} from "mongodb";
import {findVersion} from "../packages/finders";
import {ucFirst} from "../utils/string";

export function addPackage(packages: Collection, language: string, name: string, channel: string): Promise<string> {
    return packages.findOne({name, language, channel})
        .then((row) => {
            if (row === null) {
                // Find version in registry
                return findVersion(language, name)
                    .then((info) => {
                        // Insert it into the database
                        const obj = {name, language, channel, created: new Date(), ...info};
                        return packages.insertOne(obj);
                    });
            } else {
                // Row already exists
                return Promise.resolve(null);
            }
        })
        .then((data) => {
            let msg;
            if (data !== null) {
                const row = data.ops[0];
                msg = `*${row.title}* added (version: _${row.version}_)`;
            } else {
                msg = `*${name}* already in the list`;
            }
            return msg;
        })
        .catch(() => {
            return `*${name}* not found in registry`;
        });
}

export function removePackage(packages: Collection, language: string, name: string, channel: string): Promise<string> {
    return packages.findOne({name, language, channel})
        .then((row) => {
            if (row === null) {
                return Promise.resolve(null);
            } else {
                return packages.deleteMany({name, language});
            }
        })
        .then((data) => {
            const response = data === null ? `*${name}* not in the list` : `I removed *${name}*`;
            return Promise.resolve(response);
        });
}

export function listPackages(packages: Collection, channel: string): Promise<string> {
    return packages
        .find({channel}).sort({language: 1, name: 1}).toArray()
        .then((items) => {
            let text = "";
            for (const item of items) {
                text += `_${ucFirst(item.language)}_ / *${item.name}* at _${item.version}_\n`;
            }
            return Promise.resolve(text);
        });
}
