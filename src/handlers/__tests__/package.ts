import { Db } from "mongodb";

jest.mock("../../packages/finders", () => ({
    findVersion: jest.fn().mockImplementation((language) => {
        return new Promise((resolve, reject) => {
            if (language === "invalid") {
                reject(`No version finder found for ${language}`);
            } else {
                resolve({ name: "package4", title: "package4", version: 1 });
            }
        });
    }),
}));

// noinspection TsLint
const mongodb = require("mongo-mock");
import { addPackage, listPackages, removePackage } from "../package";

test("list of packages", () => {
    expect.assertions(1);

    return mongodb.MongoClient.connect("fakeurl", {}).then((db: Db) => {
        const collection = db.collection("packages");

        return collection.insertMany([
            { name: "package1", language: "test", version: "3", channel: "achannel" },
            { name: "package2", language: "test", version: "2", channel: "achannel" },
            { name: "package3", language: "test", version: "1", channel: "achannel" },
        ])
            .then(() => listPackages(collection, "achannel"));
    })
        .then((text: string) => expect(text).toBe("_Test_ / *package1* at _3_\n" +
            "_Test_ / *package2* at _2_\n" +
            "_Test_ / *package3* at _1_\n"));
});

test("remove package", () => {
    expect.assertions(2);

    return mongodb.MongoClient.connect("fakeurl2", {}).then((db: Db) => {
        const collection = db.collection("packages");

        return collection.insertMany([
            { name: "package1", language: "test", version: "3", channel: "achannel" },
            { name: "package2", language: "test", version: "2", channel: "achannel" },
            { name: "package3", language: "test", version: "1", channel: "achannel" },
        ])
            .then(() => removePackage(collection, "test", "package1", "achannel"))
            .then((text) => {
                return Promise.all([
                    text,
                    collection.find().toArray(),
                ]);
            });
    })
        .then(([text, packages]: [string, string[]]) => {
            expect(text).toBe("I removed *package1*");
            expect(packages.length).toBe(2);
        });
});

test("add package", () => {
    expect.assertions(1);

    return mongodb.MongoClient.connect("fakeurl3", {}).then((db: Db) => {
        const collection = db.collection("packages");
        return addPackage(collection, "test", "package4", "achannel");
    })
        .then((text: string) => {
            expect(text).toBe("*package4* added (version: _1_)");
        });
});

test("add dupe package", () => {
    expect.assertions(1);
    const fakePackage = {
        created: new Date(), language: "test", name: "package4",
        releaseUrl: "http://test",  title: "package4", version: "1",
    };

    return mongodb.MongoClient.connect("fakeurl3", {})
        .then((db: Db) => db.collection("packages").insertOne(fakePackage).then(() => db))
        .then((db: Db) => {
            const collection = db.collection("packages");
            return addPackage(collection, "test", "package4", "achannel");
        })
        .then((text: string) => {
            expect(text).toBe("*package4* already in the list");
        });
});

test("add non-existent package", () => {
    expect.assertions(1);

    return mongodb.MongoClient.connect("fakeurl3", {})
        .then((db: Db) => {
            const collection = db.collection("packages");
            return addPackage(collection, "invalid", "package4", "achannel");
        })
        .then((text: string) => {
            expect(text).toBe("*package4* not found in registry");
        });
});
