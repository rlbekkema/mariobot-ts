import {rtm} from "slack";
import * as WebSocket from "ws";
import {getDB} from "./db/db";
import {handleMessage} from "./slack/message";
import {getConfig, IConfig} from "./utils/config";
import {startScanner} from "./utils/scanner";

export async function startBot(cfg: IConfig): Promise<void> {
    let msgId = 1;
    const {db} = await getDB(cfg.mongoUrl, cfg.mongoName);
    const api = await rtm.connect({token: cfg.slackApiToken});

    function sendMessage(channel: string, message: string) {
        ws.send(JSON.stringify({
            channel,
            id: msgId++,
            text: message,
            type: "message",
        }));
    }

    let scannerStarted = false;
    const ws = new WebSocket(api.url);

    ws.on("message", (data) => {
        const msg = JSON.parse(data.toString());

        switch (msg.type) {
            case "hello":
                console.log("Slack says hello");
                if (!scannerStarted) {
                    startScanner(db, cfg);
                    scannerStarted = true;
                }
                break;
            case "goodbye":
                console.log("Slack says goodbye");
                break;
            case "message":
                handleMessage(db, api.self, msg, cfg)
                    .then((responseText) => sendMessage(msg.channel, responseText))
                    .catch(() => {
                        // console.log("Skipping", msg); // Probably nothing to do
                    });
        }
    });

    ws.on("close", async (code, reason) => {
        console.log(`Connection closed becase "${reason}" (${code})`);
        process.exit();
    });

    ws.on("error", (error) => {
        console.error(error);
        process.exit();
    });

    db.on("close", () => {
        console.log("Mongo closed");
        process.exit();
    });

    db.on("error", (error) => {
        console.error(error);
        process.exit();
    });
}

if (require.main === module) {
    startBot(getConfig())
        .then(() => console.log("Bot started"));
}
