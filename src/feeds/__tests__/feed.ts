import {parse} from "date-fns";
import * as nock from "nock";
import {updateFeed} from "../feed";

const fakeAtom = `<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>Test Feed</title>
    <updated>2003-12-13T18:30:02Z</updated>
    <entry>
        <title>Entry 1</title>
        <link href="http://test/article" />
        <updated>2003-12-13T18:30:02Z</updated>
        <summary>Some text.</summary>
        <content type="xhtml">
            <a href="https://testit">Content</a>
        </content>
    </entry>
</feed>`;

test("update a feed", () => {
    const fakeUrl = "http://test";

    nock(fakeUrl)
        .get("/")
        .reply(200, fakeAtom);

    expect.assertions(2);
    return updateFeed(fakeUrl)
        .then((feed) => {
            expect(feed.title).toBe("Test Feed");
            expect(feed.items).toEqual([{
                content: "Some text.",
                link: "http://test/article",
                published: parse("2003-12-13T18:30:02.000Z"),
                title: "Entry 1",
            }]);
        });
});

test("busted feed", () => {
    const fakeUrl = "http://test";

    nock(fakeUrl)
        .get("/")
        .reply(200, "invalid");

    expect.assertions(1);
    return updateFeed(fakeUrl)
        .catch((error) => {
            expect(error).toEqual(new Error("Not a feed"));
        });
});
