import * as FeedParser from "feedparser";
import fetch from "node-fetch";
import {cleanHtml} from "../utils/feed";

export interface IFeed {
    title: string;
    items: IFeedItem[];
}

export interface IFeedItem {
    title: string;
    published: Date;
    link: string;
    content: string;
}

export function updateFeed(url: string, since: Date | null = null): Promise<IFeed> {
    return new Promise((resolve, reject) => {
        const feed: IFeed = {
            items: [],
            title: "",
        };

        const feedParser = new FeedParser({});
        feedParser.on("readable", function(this: FeedParser) {
            const stream = this; // `this` is `feedparser`, which is a stream

            while (true) {
                const item = stream.read();
                if (!item) {
                    break;
                }

                const date = item.pubdate || item.date;
                if (!since || (date && date > since)) {
                    if (date && item.link) {
                        feed.items.push({
                            content: cleanHtml(item.summary || item.description),
                            link: item.link,
                            published: date,
                            title: item.title,
                        });
                    }
                }
            }
        });

        feedParser.on("meta", (meta: { title: string }) => {
            feed.title = meta.title;
        });

        feedParser.on("end", () => {
            feed.items.sort((a, b) => a.published.getTime() - b.published.getTime());
            resolve(feed);
        });

        feedParser.on("error", (error: Error) => {
            reject(error);
        });

        fetch(url)
            .then((response) => {
                response.body.on("close", () => {
                    feedParser.destroy();
                });
                response.body.on("end", () => {
                    (response.body as any).destroy();
                });
                response.body.pipe(feedParser, {end: true});
            });
    });

}
