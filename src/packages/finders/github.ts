import fetch from "node-fetch";
import * as semver from "semver";
import {IVersionInfo} from "../types";

export function findGithubVersion(name: string): Promise<IVersionInfo> {
    return fetch(`https://api.github.com/repos/${name}/tags`)
        .then((response) => response.json())
        .then((data) => {
            if (data.length === 0) {
                throw new Error("No tags in this repo");
            }

            // Sort by semver, descending order
            if (semver.valid(data[0].name)) {
                data = data.filter((i: {name: string}) => semver.valid(i.name));
                data.sort((i1: {name: string}, i2: {name: string}) => semver.rcompare(i1.name, i2.name));
            }

            const latest = data[0];
            const releaseUrl = `https://github.com/${name}/releases/tag/${latest.name}`;
            return {version: latest.name, releaseUrl, title: name};
        });
}
