import fetch from "node-fetch";
import {IVersionInfo} from "../types";

export function findPypiVersion(name: string): Promise<IVersionInfo> {
    return fetch(`http://pypi.python.org/pypi/${name}/json`)
        .then((response) => response.json())
        .then((data) => {
            const {version, release_url: releaseUrl, name: title} = data.info;
            return {version, releaseUrl, title};
        });
}
