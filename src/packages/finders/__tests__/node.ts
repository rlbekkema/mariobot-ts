import nock = require("nock");
import {findNPMVersion} from "../node";

test("add node package", async () => {
    const body = {
        "dist-tags": {latest: "1.0.1"},
        "homepage": "http://testurl",
        "name": "my-test-package",
    };
    nock("https://registry.yarnpkg.com")
        .get("/my-test-package")
        .reply(200, body);

    const version = await findNPMVersion("my-test-package");
    expect(version).toEqual({
        releaseUrl: "http://testurl",
        title: "my-test-package",
        version: "1.0.1",
    });
});

test("missing website", async () => {
    const body = {
        "dist-tags": {latest: "1.0.3"},
        "name": "my-test-package",
        "repository": {
            url: "http://repourl",
        },
    };
    nock("https://registry.yarnpkg.com")
        .get("/my-test-package")
        .reply(200, body);

    const version = await findNPMVersion("my-test-package");
    expect(version).toEqual({
        releaseUrl: "http://repourl",
        title: "my-test-package",
        version: "1.0.3",
    });
});

test("add scoped node package", async () => {
    const body = {
        "dist-tags": {latest: "1.0.2"},
        "homepage": "http://testurl2",
        "name": "@test/my-test-package",
    };
    nock("https://registry.yarnpkg.com")
        .get("/@test%2Fmy-test-package")
        .reply(200, body);

    const version = await findNPMVersion("@test/my-test-package");
    expect(version).toEqual({
        releaseUrl: "http://testurl2",
        title: "@test/my-test-package",
        version: "1.0.2",
    });
});
