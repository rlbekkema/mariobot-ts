import fetch from "node-fetch";
import {IVersionInfo} from "../types";

export async function findNPMVersion(name: string): Promise<IVersionInfo> {
    if (name[0] === "@") {
        name = `@${encodeURIComponent(name.substr(1))}`;
    }
    const info = await fetch(`https://registry.yarnpkg.com/${name}`)
        .then((data) => data.json());

    const releaseUrl = info.homepage || info.repository && info.repository.url;

    return {version: info["dist-tags"].latest, releaseUrl, title: info.name};
}
