import fetch from "node-fetch";
import * as semver from "semver";
import {IVersionInfo} from "../types";

export function findGitlabVersion(name: string): Promise<IVersionInfo> {
    const uriName = encodeURIComponent(name);
    return fetch(`https://gitlab.com/api/v4/projects/${uriName}/repository/tags`)
        .then((response) => response.json())
        .then((data) => {
            if (data.length === 0) {
                throw new Error("No tags in this repo");
            }

            // Sort by semver, descending order
            if (semver.valid(data[0].name)) {
                data.sort((i1: {name: string}, i2: {name: string}) => semver.rcompare(i1.name, i2.name));
            }

            const latest = data[0];
            const releaseUrl = `https://gitlab.com/${uriName}/tags/${latest.name}`;
            return {version: latest.name, releaseUrl, title: name};
        });
}
