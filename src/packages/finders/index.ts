import {IFinders, IVersionInfo} from "../types";
import {findBrewVersion} from "./brew";
import {findGithubVersion} from "./github";
import {findGitlabVersion} from "./gitlab";
import {findNPMVersion} from "./node";
import {findPypiVersion} from "./python";

export const packageFinders: IFinders = {
    brew: findBrewVersion,
    github: findGithubVersion,
    gitlab: findGitlabVersion,
    node: findNPMVersion,
    python: findPypiVersion,
};

export function findVersion(language: string, name: string): Promise<IVersionInfo> {
    const finder = packageFinders[language];
    if (finder) {
        return finder(name);
    } else {
        return Promise.reject(`No version finder found for ${language}`);
    }
}
