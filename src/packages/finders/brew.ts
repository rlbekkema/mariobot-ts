import fetch from "node-fetch";
import {IVersionInfo} from "../types";

export function findBrewVersion(name: string): Promise<IVersionInfo> {
    return fetch(`http://brewformulas.org/${name}.json`)
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error("Formula not found");
            }
        })
        .then((data) => {
            return {version: data.version, releaseUrl: data.homepage, title: data.formula};
        });
}
