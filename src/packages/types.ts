export interface IVersionInfo {
    title: string;
    version: string;
    releaseUrl: string;
}

export interface IFinders {
    [key: string]: (name: string) => Promise<IVersionInfo>;
}
