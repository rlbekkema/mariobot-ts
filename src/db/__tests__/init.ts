import mongodb = require("mongo-mock");
import { Db } from "mongodb";
import * as nock from "nock";
import { initDB } from "../init";

class FakeDB {
    public static mock = jest.fn();

    public collection(name: string) {
        return {
            createIndex: FakeDB.mock,
        };
    }
}

test("initialize db", async () => {
    const db = new FakeDB();

    await initDB(db as any);

    expect(FakeDB.mock.mock.calls.length).toBe(2);
});

test("init fails", () => {
    // Make it fail
    expect.assertions(1);

    const fakeDB = {
        collection: () => ({
            createIndex: () => Promise.reject(new Error("Opps")),
        }),
    };
    return initDB(fakeDB as any)
        .then((db) => {
            expect(db).toBe(fakeDB);
        })
});
