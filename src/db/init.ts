import { Db } from "mongodb";

/**
 * Initialze the database (ensure indexes etc.)
 * @param {Db} db The database to work on
 * @return {Promise<Db>}
 */
export function initDB(db: Db): Promise<Db> {
    return Promise.all([
        db.collection("packages").createIndex({ language: 1, name: 1, channel: 1 },
            { unique: true, name: "lang_name_channel_unq" }),
        db.collection("feeds").createIndex({ url: 1, channel: 1 }, { unique: true, name: "feeds_unq" }),
    ])
        .then(() => db)
        .catch((err) => {
            console.log(err);
            return db;
        });
}
