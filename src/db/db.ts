import {Db, MongoClient} from "mongodb";
import {initDB} from "./init";

export async function getDB(url: string, dbName: string): Promise<{client: MongoClient, db: Db}> {
    const client = await MongoClient.connect(url, {reconnectTries: Infinity});
    const db = await initDB(client.db(dbName));

    return {client, db};
}
