FROM node:alpine

WORKDIR /app

COPY src /app/src
COPY package.json /app
COPY yarn.lock /app
COPY tsconfig.json /app

RUN yarn install
RUN yarn tsc


FROM node:alpine

WORKDIR /app
COPY --from=0 /app/dist /app
COPY --from=0 /app/node_modules /app/node_modules
COPY wait-for /app

RUN ln -s /run/secrets/mariobot_env .env

CMD sh -c './wait-for mongo:27017 -- node bot.js'
