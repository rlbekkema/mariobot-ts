[![pipeline status](https://gitlab.com/rlbekkema/mariobot-ts/badges/master/pipeline.svg)](https://gitlab.com/rlbekkema/mariobot-ts/commits/master)
[![coverage report](https://gitlab.com/rlbekkema/mariobot-ts/badges/master/coverage.svg)](https://gitlab.com/rlbekkema/mariobot-ts/commits/master)

# MarioBot

A Slack bot that allows users to manage a list of packages for MarioBot to scan and report back on

## Supported types of packages

- Homebrew
- RSS/Atom feeds
- Github
- Gitlab
- Node
- Python

## Scanner

MarioBot will scan every 30 minutes for updates. It will simply check if the latest version / item is different 
from the current one and then store it in the database if it is and report back on it in Slack.

## Database

It uses MongoDB because that has such an amazing reputation 

## Usage

Mention `@mariobot` in a message and it will respond with a list of possible commands
